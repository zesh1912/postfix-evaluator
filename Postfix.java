import java.util.Stack;

/*
 * To finish this, add the following operations:
 * ^     Exponent
 * mod   Mod
 * dup   Take top value of stack and duplicate it
 * swap  Swap top two values
 * rot   Take 3rd item down, bring to top
 * drop  Remove top entry
 */
public class Postfix
{
    String expr;
    
    
    public Postfix(String e)
    {
        expr = e;
    }
    
    public double eval()
    {
        Stack<Double> stack = new Stack<Double>();
        String[] word = expr.split("\\s+");
        for (int i = 0; i < word.length; i++)
        {
            double a, b, c;
            switch(word[i])
            {
                case "+":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a + b);
                    break;
                case "-":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a - b);
                    break;
                case "*":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a * b);
                    break;
                case "/":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a / b);
                    break;
                case "Math.pow(a,b)":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(Math.pow(a,b));
                    break;
                case "%":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(a % b);
                    break;
                
                case "dup":
                    b = stack.pop();
                    stack.push(b);
                    stack.push(b); 
                    break;
                    
                case "swap":
                    b = stack.pop();
                    a = stack.pop();
                    stack.push(b);
                    stack.push(a);
                    break;
                
                case "rot":
                    b = stack.pop();
                    a = stack.pop();
                    c = stack.pop();
                    stack.push(b);
                    stack.push(a);
                    stack.push(c);
                    break;
                    
                case "drop":
                    b = stack.pop();
                    break;
                        
                default:
                    stack.push(Double.parseDouble(word[i]));
                    break;
            }
        }
           return stack.peek();
    }
   
}